const express = require('express');
const router = express.Router();
const passport = require('passport');
//Load Input Validation
const Question = require('../../models/written_questions');
const setting=require("../return_msg/setting");
const roleValidation=require("../validation/role");
let middleware = require('../../validation/middleware');


//@route GET api/university/register
//@desc Register route
//@access Public
router.post('/',middleware.checkToken, (req, res) => {

    console.log(req.body);
    
    if(req.body.question==undefined || req.body.question==null || req.body.question=='') 
      {
         return res.json(

            setting.status("Quesition cannot be empty",false,"question is empty",null)
    
        );
      }

    const question = new Question({
        question: req.body.question,
        
    });

    question.save()
        .then(role =>{
            res.json(

                setting.status("Question created",true,"created",role)
        
            );
        })
        .catch(err => {
            if(err.errors.question)
                {
                    res.json(

                        setting.status("question already exits",false,"question unique",null)
                
                    );
                }
            
        });    
})


//@route  GET api/university/all
//@desc  Get all  university
//@access Public

router.get('/',middleware.checkToken, (req, res,next) => {

    // var result=middleware.function1("CAN_VIEW_ROLE");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }


    // Question.find()
    //     .then(result => {
    //         res.send(
  
    //             setting.status("Show all",true,"details",result)
        
    //           );
    //     }); 

    Question.findRandom(function (err,element){
        if(err)
        {
            return res.send(

                setting.status("Error",false,"eror",err)
    
            );
        }
        else
        {
            return res.send(

                setting.status("Question",true,"Data",element)
    
            );
        }
    })
})

//@route  GET api/university/id
//@desc  Get one  university
//@access Public
router.get('/:id', (req, res) => {

    // var result=middleware.function1("CAN_VIEW_ROLE");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }


    var ObjectId = require('mongodb').ObjectID;
    const id=req.params.id;

    if(!ObjectId.isValid(id))
        {
            return res.send(
                    
                setting.status(validation.FALSE,"False","incorrect id",null)

                );
        }

    Question.find({_id:id})
        .then(result => {
            res.send(
  
                setting.status("Show all",true,"details",result)
        
              );
        });   
})

// @route   DELETE api/university/:id
// @desc    Delete university
// @access  Private
// router.delete(
//     '/:id',middleware.checkToken,
//    // passport.authenticate('jwt', { session: false }),
//     (req, res) => {

//     // var result=middleware.function1("CAN_DELETE_ROLE");
//     // if(!result.status)
//     // {
//     //     return res.send(result);
//     // }
    
//     var ObjectId = require('mongodb').ObjectID;
//     var id=req.params.id;
    
//     if(!ObjectId.isValid(id))
//         {
//             return res.send(
                    
//                 setting.status("Incorrect ID","False","incorrect id",null)

//                 );
//         }
        
//         Question.delete({_id:id})
//         .exec()
//         .then(() => {
//                 res.json(
    
//                     setting.status("Quetion deleted","True","Deleted",null)
    
//                 );
//             })
//         .catch(err => {
//             console.log(err);
//             res.json(
    
//             setting.status("Error","False","Error",err)
    
//             );
//         });    
//     });

//@route GET api/university/universityupdate
//@desc Register route
//@access Public
router.post('/:id',middleware.checkToken, (req, res) => {

    // var result=middleware.function1("CAN_EDIT_ROLE");
    // if(!result.status)
    // {
    //     return res.send(result);
    // }


    if(req.body.question==undefined || req.body.question==null || req.body.question=='') 
      {
         return res.json(

            setting.status("Question cannot be empty",false,"question is empty",null)
    
        );
      }

    var ObjectId = require('mongodb').ObjectID;

	if(!ObjectId.isValid(req.params.id))
        {
            return res.send(
                        
                setting.status("ID wrong","False","object id wrong",null)

            );
        }

    const id = req.params.id;

    Question.findOne({
            _id: id
        })
        .then(university => {
            if (university) {
                
                Question.findOneAndUpdate(
                    { _id : id },
                    {$set:{question: req.body.question}},
                    {runValidators: true, context: 'query' })
                   // {_id:id}}
                  .then(university =>{
                    res.json(
		
                        setting.status("Question Updated",true,"updated",university)
                
                      );
                  })
                  .catch(err =>{
                   return  res.json(
		
                    setting.status("Error",false,"error",err)
            
                  );
                  });
            } else {
                res.json(
		
                    setting.status("Question Not Found",false,"error",err)
            
                  );
            }
        })
})

module.exports = router;